﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.CSharp;

namespace Iodynis.Libraries.Replacing
{
    public class Replacer
    {
		private struct SequenceBlock
		{
			public readonly string Open;
			public readonly string Close;
			public SequenceBlock(string param_sequenceOpen, string param_sequenceClose)
			{
				Open = param_sequenceOpen;
				Close = param_sequenceClose;
			}
		}
		public struct SequenceReplacer
		{
			public readonly string Identificator;
			public readonly Func<string, string> Function;

			public SequenceReplacer(string param_identificator, Func<string, string> param_function)
			{
				Identificator = param_identificator;
				Function = param_function;
			}
		}

		private static readonly string[][] DefaultBlocks = { new string[] { "⌠", "⌡" }, new string[] { "-<|[", "]|>-" } };
		private static readonly char[] DefaultEmptyCharacters = {' ', '\t', '\r', '\n'};

		private readonly List<SequenceBlock> SequenceBlocks = new List<SequenceBlock>();
		private readonly Dictionary<string, Func<string, string>> SequenceReplacers = new Dictionary<string, Func<string, string>>();
		public IReadOnlyDictionary<string, Func<string, string>> Sequences
		{
			get
			{
				return SequenceReplacers;
			}
		}
		public string SequenceFile { get; }
		public string SequenceLine { get; }
		public string SequenceLines { get; }
		public string SequenceCode { get; }

		public Replacer()
			: this(DefaultBlocks) { }

		public Replacer(string sequenceOpen, string sequenceClose)
			: this(new string[][] { new string[] { sequenceOpen, sequenceClose } }) { }

		public Replacer(ICollection<SequenceReplacer> sequencesCustom)
			: this(DefaultBlocks, sequencesCustom) { }

		public Replacer(string[][] sequenceBlocks, ICollection<SequenceReplacer> sequencesCustom = null)
		{
			for (int i = 0; i< sequenceBlocks.Length; i++)
			{
				if (sequenceBlocks[i].Length != 2)
				{
					continue;
				}
				SequenceBlocks.Add(new SequenceBlock(sequenceBlocks[i][0], sequenceBlocks[i][1]));
			}

			if (sequencesCustom != null)
			{
				foreach (SequenceReplacer sequence in sequencesCustom)
				{
					if (SequenceReplacers.ContainsKey(sequence.Identificator))
					{
						throw new Exception($"Multiple sequence identificator {sequence.Identificator}.");
					}
					if (sequence.Identificator == "file" || sequence.Identificator == "line" || sequence.Identificator == "lines" || sequence.Identificator == "code")
					{
						throw new Exception($"Sequence identificator {sequence.Identificator} is a system one and is not allowed.");
					}
					SequenceReplacers.Add(sequence.Identificator, sequence.Function);
				}
			}

			SequenceFile =  "file";
			SequenceLine =  "line";
			SequenceLines = "lines";
			SequenceCode =  "code";
		}

		private bool? IsTrueOrFalse(string param_expression, Dictionary<string, string> param_dictionary)
		{
			Regex regex = new Regex(@"^\s*(?<key>[a-zA-Z0-9_\-]+)\s*(?<sign>==|!=)\s*(?<value>[a-zA-Z0-9_\-\,\s]+)\s*$");
			Match match = regex.Match(param_expression);
			// No match found - no result
			if (!match.Success)
			{
				return null;
			}
			string key = match.Groups["key"].Value;
			// No key (that's strange) - no result
			if (!param_dictionary.ContainsKey(key))
			{
				return null;
			}
			string sign = match.Groups["sign"].Value;
			// Invalid sign - no result
			if (sign != "==" && sign != "!=")
			{
				return null;
			}
			// Values may be seporated by commas
			string[] values = match.Groups["value"].Value.Split(new char[] { ',' });
			for (int i = 0; i < values.Length; i++)
			{
				values[i] = values[i].Trim(new char[] { ' ', '\t', '\r', '\n' });
			}

			// In case of '==' any match should make the statement true
			if (sign == "==")
			{
				for (int i = 0; i < values.Length; i++)
				{
					if (String.Compare(param_dictionary[key], values[i], StringComparison.Ordinal) == 0)
					{
						return true;
					}
				}
				return false;
			}
			// In case of != any match should make the statement false
			if (sign == "!=")
			{
				for (int i = 0; i < values.Length; i++)
				{
					if (String.Compare(param_dictionary[key], values[i], StringComparison.Ordinal) == 0)
					{
						return false;
					}
				}
				return true;
			}
			return null;
		}

		public int ReplaceDictionary(Dictionary<string, string> param_dictionary, int param_iterationsCount = 1, string param_code = "")
		{
			string[] keys = new string[param_dictionary.Count];
			param_dictionary.Keys.CopyTo(keys, 0);

			int replacementsCountTotal = 0;
			int replacementsCount;
			for (int iteration = 0; iteration < param_iterationsCount; iteration++)
			{
				for (int i = 0; i < keys.Length; i++)
				{
					param_dictionary[keys[i]] = ReplaceString(param_dictionary[keys[i]], param_dictionary, out replacementsCount, 1, param_code);
					replacementsCountTotal += replacementsCount;
				}
			}
			return replacementsCountTotal;
		}
		public void ReplaceFile(string param_path, Dictionary<string, string> param_dictionary, int param_iterationsCount = 1, string param_code = "")
		{
			string contents = File.ReadAllText(param_path);
			contents = ReplaceString(contents, param_dictionary, param_iterationsCount, param_code);
			File.WriteAllText(param_path, contents);
		}
		public string ReplaceString(string param_text, Dictionary<string, string> param_dictionary, int param_iterationsCount = 1, string param_code = "")
		{
			int replacementsCount;
			return ReplaceString(param_text, param_dictionary, out replacementsCount, param_iterationsCount, param_code);
		}
		public string ReplaceString(string param_text, Dictionary<string, string> param_dictionary, out int param_replacementsCount, int param_iterationsCount = 1, string param_code = "")
		{
			param_replacementsCount = 0;
			//int replacesTotal = 0;
			int replacementsDone;
			int replacementsLeft;
			//int newLineIndex;

			bool skipLines;
			bool skipLine;

			StringBuilder stringBuilder = new StringBuilder(param_text);
			StringBuilder stringBuilderLine = new StringBuilder();

			Dictionary<string, string> dictionary = new Dictionary<string, string>(param_dictionary);

			for (int iteration = 0; iteration < param_iterationsCount; iteration++)
			{
				foreach (SequenceBlock sequenceBlock in SequenceBlocks)
				{
					do
					{
						string text = stringBuilder.ToString();
						stringBuilder.Clear();
						stringBuilderLine.Clear();

						replacementsDone = 0;
						replacementsLeft = 0;

						skipLines = false;
						skipLine = false;

						int sequenceOpenLeftIndexPrevious = 0;
						int sequenceOpenRightIndexPrevious = 0;
						int sequenceCloseLeftIndexPrevious = 0;
						int sequenceCloseRightIndexPrevious = 0;
						//int lastPositionAppended;

						for (int i = 0; i < text.Length; i++)
						{
							int sequenceOpenLeftIndex = 0;
							int sequenceOpenRightIndex = 0;
							int sequenceCloseLeftIndex = 0;
							int sequenceCloseRightIndex = 0;

							// Seek for opening sequence
							while (i < text.Length)
							{
								if (text[i] == '\r' || text[i] == '\n')
								{
									if (!skipLine)
									{
										if (!skipLines)
										{
											stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, i - sequenceCloseRightIndexPrevious);
											sequenceCloseRightIndexPrevious = i; // Hack: move the closing index to the new line position, so we can dump the line now and index will show the end of whats inside stringBuilder
										}
										stringBuilder.Append(stringBuilderLine.ToString());
									}
									stringBuilderLine.Clear();
									skipLine = false;//(i + 1) < text.Length && text[i] == '\r' && text[i + 1] == '\n'; // Should revert back to false now, but not in case of a \r\n sequence
								}
								else if (String.Compare(text, i, sequenceBlock.Open, 0, sequenceBlock.Open.Length, false /* Case-insensitive */, CultureInfo.InvariantCulture) == 0)
								{
									sequenceOpenLeftIndex = i;
									sequenceOpenRightIndex = sequenceOpenLeftIndex + sequenceBlock.Open.Length;
									break;
								}
								i++;
							}
							// Seek for closing sequence
							while (i < text.Length)
							{
								// Opening sequence
								if (String.Compare(text, i, sequenceBlock.Open, 0, sequenceBlock.Open.Length, false /* Case-insensitive */, CultureInfo.InvariantCulture) == 0)
								{
									sequenceOpenLeftIndex = i;
									sequenceOpenRightIndex = sequenceOpenLeftIndex + sequenceBlock.Open.Length;
									replacementsLeft++;
								}
								// Closing sequence
								else if (String.Compare(text, i, sequenceBlock.Close, 0, sequenceBlock.Close.Length, false /* Case-insensitive */, CultureInfo.InvariantCulture) == 0)
								{
									sequenceCloseLeftIndex = i;
									sequenceCloseRightIndex = sequenceCloseLeftIndex + sequenceBlock.Close.Length;
									break;
								}
								i++;
							}
							// Nothing found
							if (sequenceOpenLeftIndex == sequenceCloseLeftIndex)
							{
								break;
							}
							// Unclosed sequence found
							if (sequenceOpenLeftIndex > sequenceCloseLeftIndex)
							{
								break;
							}
							// Found a match
							string match = text.Substring(sequenceOpenRightIndex, sequenceCloseLeftIndex - sequenceOpenRightIndex);
							match = match.Trim(DefaultEmptyCharacters);

							string identificator = Substring(match, 0, '|', false);
							//if (identificator == "database_development_password")
							//{
							//	;
							//}
							string payload = Substring(match, '|', false).Trim(DefaultEmptyCharacters);

							// File directive
							if (String.Compare(identificator, SequenceFile, StringComparison.Ordinal) == 0)
							{
								bool? isTrueOrFalse = IsTrueOrFalse(payload, dictionary);
								// Incorrect directive
								if (!isTrueOrFalse.HasValue)
								{
									if (!skipLines)
									{
										stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceCloseRightIndex - sequenceCloseRightIndexPrevious);
									}
								}
								// Correct directive
								else
								{
									if (!skipLines)
									{
										stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceOpenLeftIndex - sequenceCloseRightIndexPrevious);
									}
									if (isTrueOrFalse.Value)
									{
										skipLine = true;

										// DIRTY HACK
										// To remove line if it is the first line of the file we need to find the nearest '\n' and shift the end of the sequence beyond it:
										if (sequenceOpenLeftIndex == 0)
										{
											for (int j = i; j < text.Length; j++)
											{
												if (text[j] == '\n')
												{
													sequenceCloseRightIndex = j + 1;
													i = j + 1;
													sequenceCloseLeftIndex = sequenceCloseRightIndex - sequenceBlock.Close.Length;
													break;
												}
											}
										}
									}
									else
									{
										return null;
									}
								}
								skipLines = false;
							}
							// Multi-line directive
							if (String.Compare(identificator, SequenceLines, StringComparison.Ordinal) == 0)
							{
								string expression = payload;
								bool? isTrueOrFalse = IsTrueOrFalse(expression, dictionary);
								// Incorrect directive
								if (!isTrueOrFalse.HasValue)
								{
									if (!skipLines)
									{
										stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceCloseRightIndex - sequenceCloseRightIndexPrevious);
									}
								}
								// Correct directive
								else
								{
									if (!skipLines)
									{
										stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceOpenLeftIndex - sequenceCloseRightIndexPrevious);
									}
									if (isTrueOrFalse.Value)
									{
										skipLines = false;
									}
									else
									{
										skipLines = true;
									}
								}
								if (expression == "")
								{
									skipLines = false;
								}
							}
							// Line directives
							else if (!skipLines && !skipLine)
							{
								// Single-line directive
								if (String.Compare(identificator, SequenceLine, StringComparison.Ordinal) == 0)
								{
									string expression = payload;
									bool? isTrueOrFalse = IsTrueOrFalse(expression, dictionary);
									// Incorrect directive
									if (!isTrueOrFalse.HasValue)
									{
										stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceCloseRightIndex - sequenceCloseRightIndexPrevious);
									}
									// Correct directive
									else
									{
										if (isTrueOrFalse.Value)
										{
											stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceOpenLeftIndex - sequenceCloseRightIndexPrevious);
										}
										else
										{
											skipLine = true;
										}
									}
								}
								// Code directive
								if (String.Compare(identificator, SequenceCode, StringComparison.Ordinal) == 0)
								{
									string usings = "";
									string code = payload;

									// Collect "using" directives
									int separatorIndex = code.IndexOf("|");
									if (separatorIndex > 0)
									{
										usings = code.Substring(0, separatorIndex);
										code = code.Substring(separatorIndex + 1);
									}

									//while (code.StartsWith("using "))
									//{
									//	int newlineIndex = code.IndexOf("\n") + 1;
									//	usings += code.Substring(0, newlineIndex);
									//	code = code.Substring(newlineIndex);
									//}

									try
									{
										var cSharpCodeProvider = new CSharpCodeProvider();//new Dictionary<string, string>() { { "CompilerVersion", "v4.5" } });
										var compilerParameters = new CompilerParameters();
										compilerParameters.GenerateInMemory = true;
										compilerParameters.GenerateExecutable = true;
										//compilerParameters.ReferencedAssemblies.Add("mscorlib.dll");
										compilerParameters.ReferencedAssemblies.Add("System.dll");
										CompilerResults results = cSharpCodeProvider.CompileAssemblyFromSource(compilerParameters,
										usings + @"

											namespace Iodynis.Libraries.Replacing.Generated
											{
												public static class Script
												{
													public static void Main(string[] args)
													{
														;
													}

													public static string Run()
													{
														" + code + @"
													}

													" + param_code + @"
												}
											}");
														//Directory.SetCurrentDirectory(" + Path.GetFullPath(param_path) + @");
										results.Errors.Cast<CompilerError>().ToList().ForEach(error => Console.WriteLine(error.ErrorText));
										Assembly assembly = results.CompiledAssembly;
										Type program = assembly.GetType("Scripting.Script");
										MethodInfo main = program.GetMethod("Run", BindingFlags.Static | BindingFlags.Public);

										string result = (string)main.Invoke(null, null);
										stringBuilderLine.Append(result);
									}
									catch (Exception exception)
									{
										;
									}
								}
								// Inline directive
								else
								{
									// Append what is before the sequence
									stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceOpenLeftIndex - sequenceCloseRightIndexPrevious);

									// Custom directive
									if (SequenceReplacers.ContainsKey(identificator))
									{
										string result = SequenceReplacers[identificator].Invoke(payload);
										stringBuilderLine.Append(result);
									}
									// Replacement directive
									else if (dictionary.ContainsKey(match))
									{
										stringBuilderLine.Append(dictionary[match]);
										replacementsDone++;
									}
									// Smart directives
									else
									{
										bool fail = true;

										// Check for auto-increment ⌠something++⌡
										if (match.EndsWith("++"))
										{
											string key = match.Substring(0, match.Length - 2);
											int value;
											if (dictionary.ContainsKey(key) && Int32.TryParse(key, out value))
											{
												//stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceOpenLeftIndex - sequenceCloseRightIndexPrevious);
												stringBuilderLine.Append(value);
												dictionary[key] = (value + 1).ToString();
												replacementsDone++;
												fail = false;
											}
										}
										// Check for auto-decrement ⌠something--⌡
										else if (match.EndsWith("--"))
										{
											string key = match.Substring(0, match.Length - 2);
											int value;
											if (dictionary.ContainsKey(key) && Int32.TryParse(key, out value))
											{
												//stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, sequenceOpenLeftIndex - sequenceCloseRightIndexPrevious);
												stringBuilderLine.Append(value);
												dictionary[key] = (value - 1).ToString();
												replacementsDone++;
												fail = false;
											}
										}

										if (fail)
										{
											stringBuilderLine.Append(text, sequenceOpenLeftIndex, sequenceCloseRightIndex - sequenceOpenLeftIndex);
											replacementsLeft++;
										}
									}
								}
							}

							sequenceOpenLeftIndexPrevious = sequenceOpenLeftIndex;
							sequenceOpenRightIndexPrevious = sequenceOpenRightIndex;
							sequenceCloseLeftIndexPrevious = sequenceCloseLeftIndex;
							sequenceCloseRightIndexPrevious = sequenceCloseRightIndex;
						}
						if (!skipLines && !skipLine)
						{
							stringBuilderLine.Append(text, sequenceCloseRightIndexPrevious, text.Length - sequenceCloseRightIndexPrevious);
						}
						stringBuilder.Append(stringBuilderLine);
						param_replacementsCount += replacementsDone;
					} while (replacementsDone > 0);
				}
			}

			return stringBuilder.ToString();
		}
		private static string Substring(string @string, int index, char character, bool includeCharacter = false)
		{
			int indexOfChar = @string.IndexOf(character);
			return indexOfChar < 0 ? @string.Substring(index) : @string.Substring(index, indexOfChar - index + (includeCharacter ? 1 : 0));
		}
		private static string Substring(string @string, char character, bool includeCharacter)
		{
			int indexOfChar = @string.IndexOf(character);
			return indexOfChar < 0 ? @string : @string.Substring(indexOfChar + (includeCharacter ? 0 : 1));
		}
    }
}
